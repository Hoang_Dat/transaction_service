// // const redis = require("redis");
// // const client = redis.createClient();

// // client.on("error", function(error) {
// //   console.error(error);
// // });

// // client.set("key", "Dat");
// // client.get("key", redis.print);

// const redis = require("redis");

// const subscriber = redis.createClient();
// const publisher = redis.createClient();

// let messageCount = 0;

// // subscriber.on("subscribe", function(channel, count) {
// // //   publisher.publish("a channel", "a message");
// // //   publisher.publish("a channel", "another message");
// // });

// subscriber.on("message", function(channel, message) {
//   messageCount += 1;

//   console.log("Subscriber received message in channel '" + channel + "': " + message);

//   if (messageCount === 2) {
//     subscriber.unsubscribe();
//     subscriber.quit();
//     publisher.quit();
//   }
// });

// subscriber.subscribe("a channel");


// publisher.publish("a channel", "MESSAGES from Duong Hoang DAt");
// publisher.publish("a channel", "MESSAGES from Duong Hoang DAtdwdwdw");
// publisher.publish("a channel", "MESSAGES from Duong Hoang DAtdwdwdwsdsdsd");
// publisher.publish("a channel", "MESSAGES from Duong Hoang DAtdwdwdwsdsdsd");
// publisher.publish("a channel", "MESSAGES from Duong Hoang DAtdwdwdwsdsdsdsdsdsd");

const clientCache = require('./src/config/redisCache')
const blockchainDB = require('./src/config/database');
const { getTransactionForWallet, insertTransaction} = require('./src/service');
const { createGenesicBlock } = require('./src/handler/chainHandler');
const grpc = require('grpc')
const PROTO_PATH = './src/proto/blockchain.proto';
const protoLoader = require('@grpc/proto-loader');
const packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
const blockchain_proto = grpc.loadPackageDefinition(packageDefinition);
console.log(blockchain_proto)
const server = new grpc.Server();
server.addService(blockchain_proto.TransactionService.service, {
    GetTransaction: getTransactionForWallet,
    InsertTransaction: insertTransaction 
})

server.bind('127.0.0.1:50053',
    grpc.ServerCredentials.createInsecure());
createGenesicBlock();
console.log('Server running at http://127.0.0.1:50053')
server.start()