const grpc = require('grpc');
const EC = require('elliptic').ec;
const ec = new EC('secp256k1');
const { Transaction } = require('../data/transaction');
const protoLoader = require('@grpc/proto-loader');
const { getAllTransactionsForWallet, addTransaction, minePendingTransactions } = require('../handler/chainHandler');
const getTransactionForWallet = async (call, callback) => {
    const UserKey = ec.keyFromPrivate(call.request.address);
    const userWalletAddress = UserKey.getPublic('hex');
    const transactions = await getAllTransactionsForWallet(userWalletAddress);
    if (transactions) {
        callback(null, { transactions: transactions })
    } else {
        callback({
            code: grpc.status.NOT_FOUND,
            details: "The wallet not Exist"
        })
    }
}

const insertTransaction = async (call, callback) => {
    try {
        const clientKey = ec.keyFromPrivate(call.request.fromAddress);
        const clientWalletAddress = clientKey.getPublic('hex');
        const collaboratorKey = ec.keyFromPrivate(call.request.toAddress);
        const collaboratorWalletAddress = collaboratorKey.getPublic('hex');
        const newTransaction = new Transaction(clientWalletAddress, collaboratorWalletAddress, call.request.amount);
        newTransaction.signTransaction(clientKey);
        const result = await addTransaction(newTransaction);
        await minePendingTransactions(1000);
        callback(null, result)
    } catch (error) {
        console.log("error",error)
        callback({
            code: grpc.status.INVALID_ARGUMENT,
            details: "System BlockChain not Valid"
        })
    }

}

module.exports = {
    getTransactionForWallet,
    insertTransaction
};