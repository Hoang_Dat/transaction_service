const Schema = require("mongoose").Schema;

const BlockSchema = new Schema({
  previousHash: { type: String, required: true },
  timestamp: Number,
  transactions : [{
    fromAddress : String,
    toAddress : String,
    amount: Number,
    timestamp: Number,
    signature: { type: String, required: true }
     }],
  nonce: Number,
  hash: { type: String, required: true }
});

module.exports = BlockSchema;