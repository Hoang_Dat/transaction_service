const mongoose = require("mongoose");

module.exports = mongoose.model("Block", require("./schema"));