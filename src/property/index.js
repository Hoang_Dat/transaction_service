require("dotenv").config();

module.exports = {
  PORT: process.env.PORT,
  MONGODB_URL: process.env.MONGODB_URl,
  DIFFICULTY : process.env.DIFFICULTY,
};