const clientCache = require('../config/redisCache');
const asyncRedis = require("async-redis");
const crypto = require('crypto');
const EC = require('elliptic').ec;
const ec = new EC('secp256k1');
const asyncRedisClient = asyncRedis.decorate(clientCache);
const { Block } = require('../data/block');
const block = require("../model/block")
const laugnSettings = require('../property');
createGenesicBlock = async () => {
    const initValue = await asyncRedisClient.get("blockchain");
    if (!initValue || initValue === '') {
        let initBlock = new Block(Date.parse('2020-01-01'), [], '0');
        let chains = [initBlock];
        const resultInsert = await asyncRedisClient.set("blockchain", JSON.stringify(chains))
        if (resultInsert) {
            const firstBlock = new block(initBlock);
            await firstBlock.save();
        }
        return initBlock
    }

}

getLastBlockInChain = async () => {
    const result = await asyncRedisClient.get("blockchain");
    const chains = JSON.parse(result)
    return chains[chains.length - 1]
}
EmptyCachePendingTransaction = async () => {
    const count = await asyncRedisClient.llen("pendingTransactions");
    if (!count) return;
    for (let index = 0; index < count; index++) {
        await asyncRedisClient.lpop("pendingTransactions");
    }
}
minePendingTransactions = async (miningRewardAddress) => {
    let chains = [];
    let pendingTransactions = [];
    const resultTransactions = await asyncRedisClient.lrange("pendingTransactions", 0, -1);
    console.log("resultTransactions",resultTransactions)
    if (resultTransactions) {
        resultTransactions.forEach(ele => pendingTransactions.push(JSON.parse(ele)))
        const blockchains = await asyncRedisClient.get("blockchain");
        const chainsDatabases = await block.find({},{"_id":0,"__v":0});
        if (blockchains != '') {
            const chains = JSON.parse(blockchains);
            if (!isChainValid(chains) || !isChainValid(chainsDatabases))
                throw new Error("System BlockChain Invalid")
           
            let latestBlock = chains[chains.length - 1];
            const _block = new Block(Date.now(), pendingTransactions, latestBlock.hash);
            _block.mineBlock(Number(laugnSettings.DIFFICULTY));
            chains.push(_block);
            const resultInsert = await asyncRedisClient.set("blockchain", JSON.stringify(chains))
            const insert = new block(_block);
            const insertedBlock = await insert.save();
            if (resultInsert)
                await EmptyCachePendingTransaction();
            return resultInsert;
        }
        else throw new Error("Chains Not Found")
    }
}
addTransaction = async (transaction) => {
    if (!transaction.fromAddress || !transaction.toAddress) {
        throw new Error('Transaction must include from and to address');
    }

    if (!transaction.isValid()) {
        throw new Error('Cannot add invalid transaction to chain');
    }

    if (transaction.amount <= 0) {
        throw new Error('Transaction amount should be higher than 0');
    }
    const result = await asyncRedisClient.rpush(["pendingTransactions", JSON.stringify(transaction)])
    if (clientCache)
        return transaction;
    return null;
}

getAllTransactionsForWallet = async (address) => {
    const txs = [];
    const result = await asyncRedisClient.get("blockchain");
    const chains = JSON.parse(result)
    for (const block of chains) {
        for (const tx of block.transactions) {
            if (tx.fromAddress === address || tx.toAddress === address) {
                txs.push(tx);
            }
        }
    }
    return txs;

}
hasValidTransactions = (block) => {
    for (const tx of block.transactions) {
        if (!isValid(tx)) {
            return false;
        }
    }
    return true;
}
isValid = (transaction) => {
    const hashTransaction = crypto.createHash('sha256').update(transaction.fromAddress + transaction.toAddress + transaction.amount + transaction.timestamp).digest('hex');
    if (transaction.fromAddress === null) return true;
    if (!transaction.signature || transaction.signature.length === 0) {
        throw new Error('No signature in this transaction');
    }
    const publicKey = ec.keyFromPublic(transaction.fromAddress, 'hex');
    return publicKey.verify(hashTransaction, transaction.signature);
}
calculateHashBlock = (block) =>{
    const transactions = block.transactions.map(ele => {
        return {
            fromAddress : ele.fromAddress,
            toAddress : ele.toAddress,
            amount   : ele.amount,
            timestamp : ele.timestamp,
            signature : ele.signature
        }
    })
    return crypto.createHash('sha256').update(block.previousHash + block.timestamp + JSON.stringify(transactions) + block.nonce).digest('hex');
}
isChainValid = (chains) => {
    let initBlock = new Block(Date.parse('2020-01-01'), [], '0');
    const realGenesis = JSON.stringify(initBlock);
    
    if (realGenesis !== JSON.stringify(chains[0])) {
        return false;
    }
    for (let i = 1; i < chains.length; i++) {
        const currentBlock = chains[i];

        if (!hasValidTransactions(currentBlock)) {
            return false;
        }

        if (currentBlock.hash !== calculateHashBlock(currentBlock)) {
            return false;
        }
    }
    return true;
}
module.exports = {
    createGenesicBlock,
    getLastBlockInChain,
    minePendingTransactions,
    addTransaction,
    getAllTransactionsForWallet,
    isChainValid
};