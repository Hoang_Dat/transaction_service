var redis = require('redis');
var clientCache = redis.createClient();

clientCache.on('connect', function () {
    console.log('Redis client connected');
});

clientCache.on('error', function (err) {
    console.log('Something went wrong ' + err);
});

clientCache.set('blockchain', '', redis.print);

clientCache.del('pendingTransactions');

module.exports = clientCache